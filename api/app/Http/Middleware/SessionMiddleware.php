<?php

namespace App\Http\Middleware;

use Closure;

use DB;
use Log;

class SessionMiddleware
{
    public function handle($request, Closure $next)
    {
        $token = $request->input('_token');
        $query = DB::select("select api.fn_lumen_auth('$token') as result;");

        if ($query[0]->result == 0) {
            Log::info("Unauthorized token: " . $token);
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
