<?php

header('Access-Control-Allow-Origin: http://100moneta.com');
header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/test', function () {

  $userInfo = DB::select( DB::raw("select * from fn_get_user_details('9a62a510b489681a34a7072cadb06251940f4d22')"));
  print_r($userInfo);

  /*
  $client = new Predis\Client();
  $client->set('foo', 'bar');
  $value = $client->get('foo');
  echo $value;
    //
  */
  });

$app->group(['middleware' => 'auth'], function () use ($app) {

  $app->post('/post', 'App\Http\Controllers\PostController@incomingPOST');

});
