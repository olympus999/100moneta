<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Log;
use DB;
use \App\Events\SessionEvent;
use \App\Classes\UserBalanceClass;
use \App\Classes\UserClass;
use \App\Classes\TradeClass;
use \App\Models\Symbols_Settings;
use \App\Models\Trades_Open;

class PostController extends Controller
{

  private $token, $redis, $request;

  public function __construct()
  {
    $this->redis = new \Predis\Client();
  }

  public function incomingPOST(Request $request)
  {
    $this->request = $request;
    // Validate incoming data
    $validator = Validator::make($this->request->all(), [
        '_token' => 'required|alpha_num|max:255',
        'fn' => 'required|string|max:64',
    ]);
    if ($validator->fails()) {
        Log::Error('PostController: Validation failed');
        Log::Error('PostController: ' . print_r($validator->errors()->all(),true));
        return 0;
    }
    // Get token from input
    $this->token = $this->request->input('_token');
    $fn = $this->request->input('fn');
    Log::Info('PostController: incoming function - ' . $fn);
    call_user_func(array($this,$fn), $this->request);
  }

  public function openTrade()
  {
    $tradeClass = new TradeClass();
    $tradeClass->openOrder($this->request);

    //$event = new SessionEvent($token, $userInfo);
    //event($event);
  }

  public function getInitialUserData()
  {
    //Create a new user class
    $user = new UserClass($this->token);
    $data = $user->all();

    $this->sendData($data);

  }

  private function sendData($data)
  {
    $event = new SessionEvent($this->token, $data);
    event($event);
  }
}
