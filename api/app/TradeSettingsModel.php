<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeSettingsModel extends Model
{
    protected $table = 'trade_settings';
    protected $primaryKey = 'setting';
    protected $fillable = array('value');
    public $incrementing = false;
}
