<?php

namespace App\Console;

use DB;
use Redis;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Log;
use \App\Classes\ServerTimeClass;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

//Event real time events and clock to client
      $schedule->call(function () {
        //Fork

        $pid = pcntl_fork();

        if (!$pid) {

          //CHILD
          //Event real time events and clock to client
          $serverTimeClass = new ServerTimeClass();
          $serverTimeClass->serverTimeLoop();
          exit();
        }

      })->everyMinute();
// ---

// Generate quotes
      $schedule->call(function () {

        //Fork
        $pid = pcntl_fork();

        if (!$pid) {
          Log::info('Create Price Feed');
          //CHILD
          $loop_start_time = time();
          $loop_end_time = $loop_start_time + 62;
          while($loop_end_time > $loop_start_time){

            DB::statement( DB::raw("select api.fn_create_price_feed('XWW30');"));
            usleep(100000);
            DB::statement( DB::raw("select api.fn_create_price_feed('AU30');"));
            usleep(100000);
            DB::statement( DB::raw("select api.fn_create_price_feed('XE20');"));
            usleep(100000);
            DB::statement( DB::raw("select api.fn_create_price_feed('DE30');"));

            $loop_start_time = time();
            usleep(100000);

          };
          exit();
        }
      })->everyMinute();

//---

// Delete old quotes
      $schedule->call(function () {
          $o_ms = (time() - 60 * 60 * 24)*1000;
          DB::statement( DB::raw("DELETE from quotes where time < $o_ms "));
      })->daily();
//---
    }
}
