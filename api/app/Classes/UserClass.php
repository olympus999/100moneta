<?php

namespace App\Classes;

use DB;
use Log;

class UserClass
{

  public $data;

  public function __construct($token)
  {

    $this->data = new \stdClass();
    $this->data->user = new \stdClass();
    $this->data->user->accounts = new \stdClass();

    $userInfo = DB::select( DB::raw("select user_id from laravel.sessions where id = '$token'"));
    $this->data->user->id = $userInfo[0]->user_id;

    Log::info('UserClass: userInfo = ' . print_r($userInfo, true));
    Log::info('UserClass: userID = ' . $this->data->user->id);
  }

  //Get all user data available for web user
  public function all()
  {
    $id = $this->data->user->id;
    $userData = DB::select( DB::raw("select * from api.v_accounts_details where user_id = ' $id ' and user_default = 1;"));
    $this->data->user->name = $userData[0]->name;

    Log::info('UserClass: userData = ' . print_r($userData, true));

    foreach ($userData[0] as $key => $value)
    {
      if($key == 'id' || $key == 'account_type' || $key == 'balance')
      {
        Log::Info('UserClass: key ' . $key);
        $this->data->user->accounts->{$key} = $value;
      }
    }

    $this->data->user->dataType = 'initialUserData';

    Log::Info('UserClass: Data sent to socket: ' . print_r($this->data, true));

    return $this->data;
  }
}
