<?php

namespace App\Classes;

use Redis;
use Log;
use DB;
use \App\Events\QuotesSendEvent;

class QuotesClass
{

  protected $redis;

  public function __construct()
  {
    $this->redis = new \Predis\Client();
  }

  public function updateSymbolsQuotes()
  {
    //Check if the function has ran in the past 5 seconds, if not run the fucion, if has do nothing
    $timeS5 = time() * 1000 - 5 * 1000;
    $loopQuotesTime = $this->redis->get('loopTime_QuotesClass');
    if($loopQuotesTime < $timeS5)
    {
      $id = time();
      $pid = pcntl_fork();

      if (!$pid) {
        $quotesSendEvent = new QuotesSendEvent();

        while(true){

          $query = DB::select('select * from api.v_quotes_latest_enabled');

          $quotesSendEvent->quotes($query);
          event($quotesSendEvent);

          $this->checkLoopExitCriteria($id);
          $milliseconds = round(microtime(true) * 1000);
          $this->redis->set('loopTime_QuotesClass', $milliseconds);
          usleep(200000);

        };
      };
    };
  }

  protected function checkLoopExitCriteria($id)
  {
    $loopStatus = $this->redis->get('loopStatus_QuotesClass');

    if($loopStatus == '0')
    {
      Log::alert("Quitting quotes loop. ID = " . $id);
      exit();
    }
  }
}
