<?php

namespace App\Classes;

use DB;
use Log;
use Illuminate\Support\Facades\Validator;

use \App\Classes\UserBalanceClass;
use \App\Classes\UserClass;

use \App\Models\Symbols_Settings;
use \App\Models\Trades_Open;
use \App\Models\Trades_Closed;
use \App\Models\Quotes_Latest_Ticks;

class TradeClass
{

  public function openTrade($request)
  {

    // Validate incoming data
    $validator = Validator::make($request->all(), [
        '_token' => 'required|alpha_num|max:255',
        'symbol' => 'required|alpha_num|max:16',
        'expiration' => 'required|alpha_num|integer|min:30',
        'amount' => 'required|alpha_num|integer|min:1',
        'orderType' => 'required|alpha_num|integer|max:2',
        'account' => 'required|alpha_num|integer',
    ]);

    // Get symbol data to verify expiration date
    $query = Symbols_Settings::where('enabled', 1)->where('symbol', $request->input('symbol'))->first();

    //Get user input
    $token = $request->input('_token');
    $expiration = $request->input('expiration');

    //Log
    Log::Info('PostController: Symbol settings for order - ' . print_r($query, true));

    //Get allowed expirations
    $allowedExpirations = explode(',' , $query->expiration);

    //Check if expiration allowed
    if( ! in_array($request->input('expiration'), $allowedExpirations))
    {
      Log::Error('PostController: Incorrect expiration date');
      return 0;
    }

    if ($validator->fails()) {
        Log::Error('PostController: openTrade Validation failed');
        Log::Error('PostController: ' . print_r($validator->errors()->all(),true));
        return 0;
    }

    $user = new UserClass($request->input('_token'));
    Log::Info('PostController: Open trade for user with ID = ' . $user->data->user->id);

    //$data =  $request->only(['symbol', 'expiration', 'amount', 'orderType', 'account']);
    $data =  $request->all();

    // Set variables for incoming data
    Log::Info('TradeClass: Incoming data = ' . print_r($data, true));

    $data['expiration'] = ceil( time() / 60 ) * 60 + $data['expiration'];
    //$expirationTime = ceil( time() / 60 ) * 60 + $data['expiration'];
    $payout = 88;
    $data['amount'] = - $data['amount'] * 100;

    // Get user information from DB according to the token
    Log::Info('TradeClass: Info: ' . $data['_token']);
    Log::Info('TradeClass: Info: ' . $data['expiration']);
    $token = $data["_token"];
    $userInfo = DB::select( DB::raw("select * from api.fn_get_user_details('$token')"));
    Log::info("User details = " . json_encode($userInfo));
    $userInfo = $userInfo[0];
    $userID = $userInfo->id_;
    Log::info("User details = " . $userID);
    //Log::info("User details = " . json_encode($userInfo));
    //Log::info($userInfo->id_);
    //Log::info("Test = " . $userInfo['id_']);

/*
    // Verify that user has the balance needed for opening a trade
    if($accountBalance < $tradeAmount)
    {
      Log::Error('PostController: Not enough money. User balance lover than tradesize. ID = ' . $userID . '. Account balance = ' . $accountBalance . '. tradeAmount = ' . $tradeAmount);
      Return 0;
    }
*/
    //Verify that expiration date is correct
    if(($data['expiration'] < (time() - 30)) || $data['expiration'] > (time() + 30 * 60)){
      Log::Error('PostController: Incorrect expirationTime = ' . $data['expiration'] . '. Server time = ' . time() . '. ID = ' . $userID);
      Return 0;
    }

    try{
        $this->returnOpenTrade = DB::select( DB::raw("select * from api.fn_open_trade('$userID', '" . $data['account'] ."', '" . $data['symbol'] ."', '" . $data['amount'] . "', '" . $data['expiration'] . "', '" . $data['orderType'] ."')"));
    }
    catch(\Exception $e){
        Log::error('PostController: error = ' . $e);
        return 0;
    }
    $UserBalanceClass = new UserBalanceClass();
    $UserBalanceClass->balanceTransaction($data['account'], $data['amount']);


    //$this->openTrade2($userID, $symbol, $tradeAmount, $expirationTime, $orderType);
  }

  public function closeFullMinuteOrders($m)
  {

    $arrExpiredOrdersInsert = [];
    $deleteFromOpenOrdersID = [];
    $orders = [];

    $dt = new \DateTime("@$m");

    $expiredOrders = Trades_Open::where('expiration_time', $dt)->get();

    //Create array of prices
    $price = [];
    $expirationQuotes = Quotes_Latest_Ticks::all();
    foreach ($expirationQuotes as $row) {
      $price[$row->symbol] = $row->bid;
      # code...
    }

    Log::Info("NewMinuteClass: price = " . print_r($price, true));
    Log::Info("NewMinuteClass: expirationQuotes = " . print_r($expirationQuotes, true));

    foreach ($expiredOrders as $orders) {

      $arrExpiredOrdersInsert[] = [
        'id' => $orders->id,
        'account' => $orders->account,
        'user_id' => $orders->user_id,
        'symbol' => $orders->symbol,
        'amount' => $orders->amount,
        'payout' => $orders->payout,
        'open_time' => $orders->open_time,
        'expiration_time' => $orders->expiration_time,
        'open_price' => $orders->open_price,
        'order_type' => $orders->order_type,
        'close_price' => (int)$price[$orders->symbol],
      ];
      $deleteFromOpenOrdersID[] = $orders->id;
      //Log::Info('NewMinuteClass: ID - '. $orders->id);
      //Log::Info('NewMinuteClass: symbol - '. $orders->symbol);
    }



    Log::Info('NewMinuteClass: arrExpiredOrdersInsert - '. print_r($arrExpiredOrdersInsert, true));

    //Copy orders to table trades_closed
    DB::table('api.trades_closed')->insert($arrExpiredOrdersInsert);
    //Remove copied trades from trades_open
    DB::table('api.trades_open')->whereIn('id', $deleteFromOpenOrdersID)->delete();

    //Correct balances
    $UserBalanceClass = new UserBalanceClass();
    foreach ($arrExpiredOrdersInsert as $orders) {
      // If close price lower
      if($orders['open_price'] > $orders['close_price']){
        Log::Info('NewMinuteClass: Close price lower than open price - '. $orders['id']);
        if($orders['order_type'] == 0){
          Log::Info('NewMinuteClass: Close winning order id - '. $orders['id']);
          //Position closed with a win
          $amt = ((-$orders['amount']) * (1 + ($orders['payout']/100)));
          Log::Info('NewMinuteClass: Close winning order amount - ' . $amt);
          $UserBalanceClass->balanceTransaction($orders['account'], $amt);
        }
        else if($orders['order_type'] == 1){
          //Position closed with a loss
        }
      }
      // If close price higher
      else if($orders['open_price'] < $orders['close_price']){
        if($orders['order_type'] == 0){
          //Position closed with a loss
        }
        else if($orders['order_type'] == 1){
          //Position closed with a win
          $UserBalanceClass->balanceTransaction($orders['account'], ((-$orders['amount']) * (1 + ($orders['payout']/100))));
        }
      }
      // If close price equal to open price
      else if($orders['open_price'] == $orders['close_price']){
        $UserBalanceClass->balanceTransaction($orders['account'], ((-$orders['amount']) ));
      }
    }


    //$expiredOrders = DB::select( DB::raw("select * from api.trades_open where expiration_time = to_timestamp( $this->m );"));
/*
    foreach ($expiredOrders as $order) {

      echo "Value: $order<br />\n";
    }
*/

    //Log::Info("NewMinuteClass: expiredOrders = " . print_r($expiredOrders, true));
  }
}
