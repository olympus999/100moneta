<?php

namespace App\Classes;

use DB;
use Redis;
use Log;

use \App\Events\ClientsTimeEvent;
use \App\TradeSettingsModel;
use \App\Events\QuotesSendEvent;
use \App\Classes\TradeClass;


class NewMinuteClass
{

  private $m;

  // New second, declare all actions related to new second here
  public function newMinute($t)
  {

    // Get full minute
    $this->m = round( $t / (1000 * 60) ) * 60;
    Log::Info('NewMinuteClass: New Minute = ' . $this->m);

    $this->closeExpiredOrders();

    // $this->clientUpdateOrderExpirationOptions();
    // $this->serverSetOrderExpirationOptions();
  }

  // Protected functions to be called from above

  protected function closeExpiredOrders()
  {
    $tradeClass = new tradeClass();
    $tradeClass->closeFullMinuteOrders($this->m);
  }

  public function returnUpdateOrderExpirationOptions()
  {
    $settings = TradeSettingsModel::find("orderExpirationMinutes");
    return $settings;
  }

  protected function clientUpdateOrderExpirationOptions()
  {
    $settings = $this->returnUpdateOrderExpirationOptions();
    $clientTimeEvent = new ClientsTimeEvent();
    $clientTimeEvent->updateOrderExpiration($settings);
    //Send time to client
    event($clientTimeEvent);
  }

  protected function serverSetOrderExpirationOptions()
  {
    TradeSettingsModel::updateOrCreate(
       ['setting' => "orderExpirationMinutes"],
       ['value' => "{1,2,3,4,5,10,15,20,25,30}"]
    );
  }
}
