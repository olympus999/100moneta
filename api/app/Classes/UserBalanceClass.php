<?php

namespace App\Classes;

use Log;
use DB;

class UserBalanceClass
{

  public function increaseAccountBalance($id, $amount)
  {
    balanceTransaction($id, $amount);
  }

  public function decreaseAccountBalance($id, $amount)
  {
    $amount = - $amount;
    balanceTransaction($id, $amount);
  }

  // balance transcation
  public function balanceTransaction($id, $amount)
  {
    DB::select( DB::raw("update api.users_accounts set balance = balance + '$amount' where id = '$id'"));
    Log::info('UseBalanceClass: Updated');
  }
}
