<?php

namespace App\Classes;

use Redis;
use Log;
use DB;

use \App\Models\Symbols_Settings;

class SymbolsClass
{
  protected $redis;

  public function __construct()
  {
    $this->redis = new \Predis\Client();
  }

  // Get enabled symbols from DB and store in Redis
  public function updateEnabledSymbolsToRedis()
  {
    $query = Symbols_Settings::where('enabled', 1)->get();

    $query = json_encode($query);

    $this->redis->set('symbolsSettings', $query);
  }
}
