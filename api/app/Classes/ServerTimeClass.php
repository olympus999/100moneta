<?php

namespace App\Classes;

use Log;
use \App\Classes\NewSecondClass;
use \App\Classes\New10SecondClass;
use \App\Classes\NewMinuteClass;
use \App\Classes\RedisClass;
use \App\TradeSettingsModel;

class ServerTimeClass
{

  protected $nextFullSecond, $next10Second, $nextFullMinute,$quotesSendEvent,
    $newSecondClass, $new10SecondClass, $newMinuteClass, $redis;

  // Set needed classes
  public function __construct()
  {
    $this->newSecondClass = new NewSecondClass();
    $this->new10SecondClass = new new10SecondClass();
    $this->newMinuteClass = new NewMinuteClass();
    $this->redis = new RedisClass();
    $this->redis = $this->redis->obj();

    //$this->redis = new \Predis\Client();
  }

  // Emit event every second and minute
  public function serverTimeLoop()
  {
    $timeS5 = time() * 1000 - 5 * 1000;
    //$app('redis')->put('foo', 'bar');
    Log::alert("ServerTimeClass: loopTime_ServerTimeClass = " . $this->redis->get('loopTime_ServerTimeClass') . ". timeS = " . $timeS5);
    if($this->redis->get('loopTime_ServerTimeClass') < $timeS5)
    {
      $pid = pcntl_fork();
      if (!$pid) {

        //CHILD
        $id = time();
        $timeUnixMs = round(microtime(true) * 1000);
        $this->nextFullMinute = ((floor(($timeUnixMs/1000)/60))*60 + 60) * 1 * 1000;
        $this->nextFullSecond = (floor($timeUnixMs/1000) + 1) * 1000;
        $this->next10Second   = floor($timeUnixMs/10000) * 10000;

        //LOOP
        while(true){

          $timeUnixMs = round(microtime(true) * 1000);

          // Check when new second occurs
          if($this->nextFullSecond < $timeUnixMs )
          {
            //Log::Info ("ServerTimeClass: " . $timeUnixMs);
            $this->newSecond($timeUnixMs);
          }

          // Check when new 10 second occur
          if($this->next10Second < $timeUnixMs )
          {
            $this->new10Second($timeUnixMs);
          }

          // Check when new minute occurs
          if($this->nextFullMinute < $timeUnixMs )
          {
            $this->newMinute($timeUnixMs);
          }

          $this->redis->set('loopTime_ServerTimeClass', $timeUnixMs);

          //EXIT CRITERIA - Exit loop if redis loopStatus = 1
          $this->checkLoopExitCriteria($id);

          usleep(10000);
        }
        exit();
      }
    }
  }

  protected function checkLoopExitCriteria($id)
  {
    $loopStatus = $this->redis->get('loopStatus_ServerTimeClass');
    if($loopStatus == '0')
    {
      Log::alert("ServerTimeClass: intval(redis->get('loopStatus_ServerTimeClass')) = " . $this->redis->get('loopStatus_ServerTimeClass'));
      Log::alert("ServerTimeClass: Quitting. ID = " . $id);
      exit();
    }
  }

  protected function newMinute($t)
  {
    $this->nextFullMinute += 60*1000;
    $this->newMinuteClass->newMinute($t);
  }

  protected function new10Second($t)
  {
    $this->next10Second += 10*1000;
    $this->new10SecondClass->new10Second($t);
    //$newSecondEvent->
  }

  protected function newSecond($t)
  {
    $this->nextFullSecond += 1*1000;
    $this->newSecondClass->newSecond($t);
    //$newSecondEvent->
  }

}
