<?php

namespace App\Classes\Objects;

use Log;

class OrderRecord
{
  public id, account, user_id, symbol, amount, payout, open_time, expiration_time, open_price, order_type, close_price;
}
