<?php

namespace App\Classes\Validations;

use Log;
use DB;
use Illuminate\Support\Facades\Validator;

use \App\Models\Symbols_Settings;

class OpenOrderValidate
{
  public $validated;
  private $request, $symbolSettings;

  public function validate($request)
  {
    $this->request = $request;
    if($this->intialValidation())
    {
      if($this->finalValidation())
      {
        $this->validated = 1;
        return 1;
      }
    }
    $this->validated = 0;
    return 0;
  }
  private function intialValidation()
  {
    //Log::Info("OpenOrderValidate: this->request " . print_r($this->request, true ));
    $validator = Validator::make($this->request->all(), [
        '_token' => 'required|alpha_num|max:255',
        'symbol' => 'required|alpha_num|max:16',
        'expiration' => 'required|alpha_num|integer|min:30',
        'amount' => 'required|alpha_num|integer|min:1',
        'orderType' => 'required|alpha_num|integer|max:2',
        'account' => 'required|alpha_num|integer',
        'payout' => 'required|integer|min:1|max:100',
    ]);
    if ($validator->fails()) {
        Log::Error('OpenOrderValidate: Initial validation failed');
        Log::Error('OpenOrderValidate: Error: ' . print_r($validator->errors()->all(),true));
        return 0;
    }
    return 1;
  }
  private function finalValidation()
  {
    if($this->validateSymbol() && $this->validateExpiration() && $this->valideOrderType() && $this->validateAmount() && $this->validateAccount() && $this->validatePayout())
       {
         return 1;
       }
       return 0;
  }
  // Verify that symbol exits and is enabled
  private function validateSymbol()
  {
    $this->symbolSettings = Symbols_Settings::where('enabled', 1)->where('symbol', $this->request->input('symbol'))->first();
    if($this->symbolSettings == NULL)
    {
      Log::Error('OpenOrderValidate: Symbol does not exist or is disabled: ' . $this->request->input('symbol'));
      return 0;
    }
    return 1;
  }
  // Check if request expiration allowed
  private function validateExpiration()
  {
    if(!in_array($this->request->input('expiration'), explode(',' , $this->symbolSettings->expiration)))
    {
      Log::Error('OpenOrderValidate: Request expiration not allowed: ' . $this->request->input('expiration'));
      return 0;
    }
    return 1;
  }
  // Check if ordetype allowed
  private function valideOrderType()
  {
    if(!in_array($this->request->input('orderType'), array(0,1)))
    {
      Log::Error('OpenOrderValidate: Incorrect ordertype: ' . $this->request->input('orderType'));
      return 0;
    }
    return 1;
  }
  // Check if amount is allowed
  private function validateAmount()
  {
    if($this->request->input('amount') < 1)
    {
      Log::Error('OpenOrderValidate: Incorrect amount: ' . $this->request->input('amount'));
      return 0;
    }
    return 1;
  }
  // Check if account belongs to this token
  private function validateAccount()
  {
    $ret = DB::select( DB::raw("select * from api.fn_verify_account_with_token('" . $this->request->input('account') . "', '" . $this->request->input('_token') . "')"));
    if(!$ret)
    {
      Log::Error('OpenOrderValidate: Account does not match token: ' . $this->request->input('_token') . '; Account = ' . $this->request->input('account'));
      return 0;
    }
    return 1;
  }
  // Check if payout is allowed
  private function validatePayout()
  {
    if($this->symbolSettings->payout != $this->request->input('payout'))
    {
      Log::Error('OpenOrderValidate: User payout incorrect, user payout: ' . $this->request->input('payout'));
      return 0;
    }
    return 1;
  }
}
