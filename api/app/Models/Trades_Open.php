<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trades_Open extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api.trades_open';
}
