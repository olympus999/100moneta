<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Symbols_Settings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings.symbol';
}
