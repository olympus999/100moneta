<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quotes_Latest_Ticks extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api.quotes_latest_ticks';
}
