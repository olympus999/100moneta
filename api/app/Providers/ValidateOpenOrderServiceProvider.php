<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidateOpenOrderServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind('App\Classes\Validations\OpenOrderValidate', function(){

        return new OpenOrderValidate();

      });
    }

    public function provides()
    {
        return ['App\Classes\Validations\OpenOrderValidate'];
    }
}
