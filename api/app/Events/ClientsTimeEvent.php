<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ClientsTimeEvent extends Event implements ShouldBroadcast
{
  use SerializesModels;

  public $time;
  public $fn;
  public $subChannel;

  public function __construct()
  {
      $this->time = round(microtime(true) * 1000);
      $this->subChannel = 'servertime';
  }

  public function timeToClient()
  {
      $this->fn = 'updateClientsTime';
  }

  public function broadcastOn()
  {
      return ['channel'];
  }
}
