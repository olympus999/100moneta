<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

class SessionEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $subChannel;
    public $data;
    public $fn;

    public function __construct($token, $data)
    {
        $this->fn = 'sessionBased';
        $this->subChannel = $token;
        $this->data = $data;
    }

    public function broadcastOn()
    {
        return ['channel'];
    }
}
