

$(document).ready(function() {


  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }

});

function setOrderToggleOptions(){

  var j, i = 0;

  for(j = 0; j < 5; j++){
    //console.log("j = " + j);
    $( "#dropdownlist" ).append( '<div class="expoption"></div>' );
  }

  $( ".expoption" ).each(function( index ) {
    console.log( index + ": " + $( this ).text(i) );
    i++;
  });
  /*
  for(var i = 0; i < numItems; i++){
    $('div.expoption').text(i);
  }*/
}

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function toggleOrderExpiration() {
    document.getElementById("dropdownlist").classList.toggle("show");
}

function retTime(ms){
  // Create a new JavaScript Date object based on the timestamp
  //console.log("ms = " + ms);
  var date = new Date(ms);
  // Hours part from the timestamp
  var hours = date.getHours();
  // Minutes part from the timestamp
  var minutes = "0" + date.getMinutes();
  // Seconds part from the timestamp
  var seconds = "0" + date.getSeconds();

  // Will display time in 10:30:23 format
  var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  //console.log("ftime = " + formattedTime);
  return formattedTime;
}
