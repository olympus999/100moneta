<?php

use Illuminate\Database\Seeder;

class AccountTableSeeder extends Seeder
{

public function run()
{
    DB::table('accounts')->delete();
    DB::table('accounts')->insert([
        'name'     => 'Chris Sevilleja',
        'username' => 'sevilayha',
        'email'    => 'chris@scotch.io',
        'password' => Hash::make('awesome'),
    ]);
}

}