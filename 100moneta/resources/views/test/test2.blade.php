@extends('layouts.master')

@section('footer')
    <script src="/js/lib/jquery-2.2.4.min.js"></script>
    <script src="/js/lib/socket.io-1.2.0.js"></script>
    <script src="/js/javascript.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <script>

    $(document).ready(function() {
      // GLOBAL variables
      var socket = io.connect('http://52.29.223.24:3000'),
        globalTime;

      socket.on('connect', function() {
        getUserData();
      });

      socket.on("channel:{{ \Session::GetID() }}", function(message){
        // Convert text to JSON
        message = message.data.data;
        console.log("message = " + JSON.stringify(message));
        if(message.dataFromServer == "initialUserData"){
          window[message.dataFromServer](message);
          $('#userData').text(JSON.stringify(message));
          console.log('works');
        }
        // message = JSON.parse(message.quotes.data);
        //console.log("message = " + message[0].bid);
      });

      socket.on("channel:quotes", function(message){
        // Convert text to JSON
        //console.log(message);
        $('.quotes-' + message.symbol).text(message.bid);
      });

      socket.on("channel:servertime", function(message){
          if(message.fn == "updateClientsTime"){
            // Set global time
            globalTime = message.time;
            // Update time at clients
            message = retTime(globalTime);
            $('#timeunix').text(message);
          }
          else if(message.fn == "updateOrderExpiration"){
            console.log(message.value);
            $('#expiration').text(message.value);
          }
      });

      function postData(obj){
          console.log("Data posted");
          console.log(obj);

          $.ajax({
              url: 'http://100moneta.com:7777/post',
              type: 'POST',
              data: obj,
              async: true,
              beforeSend: function() {
                console.log("sending...");
              },
              success: function(data) {
                console.log("sent");
              }
          });
      }

      function object () {
        object.prototype._token = "{{ \Session::GetID() }}";
      };

      $(".join-room-quotes").click(function() {
        joinRoomSymbol = $(this).attr ( "class" ).split(" ");
        joinRoomSymbol = joinRoomSymbol[0];
        console.log("join room " + joinRoomSymbol);
        socket.emit('symbol', joinRoomSymbol);
      });

      $("#buy").click(function() {
        obj = new object();
        obj.fn = 'openTrade';
        obj.account = 7;
        obj.symbol = 'AU30';
        obj.expiration = 60;
        obj.amount = 2;
        obj.orderType = 1;
        postData(obj);
        console.log('obj = ' + JSON.stringify(obj));
        console.log('Server time = ' + Math.round(globalTime/1000));
      });

      $("#sell").click(function() {
        obj = new object();
        obj.fn = 'openTrade';
        obj.account = 7;
        obj.symbol = 'AU30';
        obj.expiration = 60;
        obj.amount = 2;
        obj.orderType = 0;
        postData(obj);
        console.log('obj = ' + JSON.stringify(obj));
        console.log('Server time = ' + Math.round(globalTime/1000));
      });

      function getUserData() {
        obj = new object();
        obj.fn = 'getInitialUserData';
        obj.symbol = 'AU30';
        console.log('obj = ' + JSON.stringify(obj));
        postData(obj);
      }
      //getUserData();
    });

    function initialUserData(message) {
      console.log('Working1');
      console.log('Message = ' + JSON.stringify(message));
      $('#account-balance').text(message.balance);
      $('#account-id').text(message.id);
    }

    </script>

    <p id="power" class="quotes-XWW30">0</p>
    <p id="power" class="quotes-AU30">0</p>

    <button class="XWW30 join-room-quotes">XWW30</button>
    <button class="AU30 join-room-quotes">AU30</button>
    <button id='sell'>Sell Trade</button>
    <button id='buy'>Buy Trade</button>

    <div id="order-expiration" class="1">

    <div id="time"></div>
    <div id="timeunix"></div>
    <div id="expiration"></div>
    <div id="divtest"></div>
    <div id="userData"></div>

    <div id="account-balance"></div>
    <div id="account-id"></div>

    {!! Helper::GenerateSession('this is how to use autoloading correctly!!') !!}

    <div class="dropdown">
    <button onclick="toggleOrderExpiration()" class="dropbtn expoption">Dropdown</button>
      <div id="dropdownlist" class="dropdown-content">
      </div>
    </div>

@stop
