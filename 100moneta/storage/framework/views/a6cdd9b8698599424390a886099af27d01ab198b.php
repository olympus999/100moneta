<?php $__env->startSection('footer'); ?>
    <script src="/js/lib/jquery-2.2.4.min.js"></script>
    <script src="/js/lib/socket.io-1.2.0.js"></script>
    <script src="/js/javascript.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <script>

    $(document).ready(function() {
      // GLOBAL variables
      var socket = io.connect('http://52.29.223.24:3000'),
        globalTime;

      socket.on('connect', function() {
        var obj = new object();
        obj.socket = socket.io.engine.id;
        postData(obj);
      });

      socket.on("channel:<?php echo e(\Session::GetID()); ?>", function(message){
        // Convert text to JSON
        console.log("message = " + JSON.stringify(message));
        // message = JSON.parse(message.quotes.data);
        console.log("message = " + message[0].bid);

        var msg = message.length;
        for(j = 0; j < msg; j++){
          console.log("message[j].symbol = " + message[j].symbol);
          console.log("msg = " + msg);
          if ($(".quotes-" + message[j].symbol).length > 0) {
              // Do stuff with $(".Mandatory")
              $(".quotes-" + message[j].symbol).each(function() {
                  // "this" points to current item in looping through all elements with
                  // class="Mandatory"
                  $(this).text(message[j].bid);
              });
          }
        }

        $('#power').text(message.bid);
      });

      socket.on("channel:quotes", function(message){
        // Convert text to JSON
        message = JSON.parse(message.quotes.data);
        console.log(message);
        var msg = message.length;
        for(j = 0; j < msg; j++){

          if ($(".quotes-" + message[j].symbol).length > 0) {
              // Do stuff with $(".Mandatory")
              $(".quotes-" + message[j].symbol).each(function() {
                  // "this" points to current item in looping through all elements with
                  // class="Mandatory"
                  $(this).text(message[j].bid);
              });
          }
        }

        $('#power').text(message.bid);
      });

      socket.on("channel:servertime", function(message){
          if(message.fn == "updateClientsTime"){
            // Set global time
            globalTime = message.time;
            // Update time at clients
            message = retTime(globalTime);
            $('#timeunix').text(message);
          }
          else if(message.fn == "updateOrderExpiration"){
            console.log(message.value);
            $('#expiration').text(message.value);
          }
      });

      function postData(obj){
          console.log("Data posted");
          console.log(obj);

          $.ajax({
              url: 'http://100moneta.com:7777/post',
              type: 'POST',
              data: obj,
              async: true,
              beforeSend: function() {
                console.log("sending...");
              },
              success: function(data) {
                console.log("sent");
              }
          });
      }

      function object () {
        object.prototype._token = "<?php echo e(\Session::GetID()); ?>";
      };

      $("#but").click(function() {
        var obj = new object();
        postData(obj);
      });
    });

    </script>

    <p id="power" class="quotes-XWW30">0</p>
    <p id="power" class="quotes-AU30">0</p>
    <button id="but">Click me</button>
    <div id="time"></div>
    <div id="timeunix"></div>
    <div id="expiration"></div>
    <div id="divtest"></div>

    <div class="dropdown">
    <button onclick="toggleOrderExpiration()" class="dropbtn expoption">Dropdown</button>
      <div id="dropdownlist" class="dropdown-content">
      </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>