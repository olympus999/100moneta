

<?php $__env->startSection('content'); ?>
    <p id="power">0</p>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script src="/js/lib/socket.io-1.2.0.js "></script>
    <script>
        //var socket = io('http://localhost:3000');
        var socket = io('http://52.29.223.24:3000/my-namespace');
        socket.on("test-channel:App\\Events\\EventName", function(message){
            // increase the power everytime we load test route
            $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>