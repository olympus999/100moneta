<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewSecondEvent extends Event
{
    use SerializesModels;

    public $unix;

    public function __construct()
    {
      $this->unix = round(microtime(true) * 1000);
    }

    public function broadcastOn()
    {
        return [];
    }
}
