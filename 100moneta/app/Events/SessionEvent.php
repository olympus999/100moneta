<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

class SessionEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $subChannel;

    public function __construct($token)
    {
        $this->subChannel = $token;
    }

    public function broadcastOn()
    {
        return ['channel'];
    }
}
