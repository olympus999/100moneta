<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

class QuotesSendEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $quotes;
    public $subChannel;

    public function __construct()
    {

    }

    public function quotes($q)
    {
      $this->quotes["data"] = json_encode($q);
      $this->subChannel = "quotes";
    }

    public function broadcastOn()
    {
        return ['channel'];
    }
}
