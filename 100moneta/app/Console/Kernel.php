<?php

namespace App\Console;

use DB;
use Redis;
use App\quote as quotes;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Log;
use \App\Classes\ServerTimeClass;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

/*
//Event real time events and clock to client
      $schedule->call(function () {
        //Fork

        $pid = pcntl_fork();

        if (!$pid) {

          //CHILD
          //Event real time events and clock to client
          $serverTimeClass = new ServerTimeClass();
          $serverTimeClass->serverTimeLoop();
          exit();
        }

      })->everyMinute();
// ---
*/
// Generate quotes
      $schedule->call(function () {

        //Fork
        $pid = pcntl_fork();

        if (!$pid) {
          //CHILD
          $loop_start_time = time();
          $loop_end_time = $loop_start_time + 62;
          while($loop_end_time > $loop_start_time){

            DB::statement( DB::raw("select fn_create_price_feed('XWW30');"));
            usleep(100000);
            DB::statement( DB::raw("select fn_create_price_feed('AU30');"));
            usleep(100000);
            DB::statement( DB::raw("select fn_create_price_feed('X');"));
            usleep(100000);
            DB::statement( DB::raw("select fn_create_price_feed('AU30');"));

            $loop_start_time = time();
            usleep(100000);

          };
          exit();
        }
      })->everyMinute();

//---

// Delete old quotes
      $schedule->call(function () {
          $o_ms = (time() - 60 * 60 * 24)*1000;
          DB::statement( DB::raw("DELETE from quotes where time < $o_ms "));
      })->daily();
//---



// Quotes in redis
        /*
        $schedule->call(function () {

//Check if prices are in Redis memory

          $values = Redis::get('XWW30');
          $values = json_decode($values);

          if(count($values) >= 498){
              // only update quotes
          }
          else {
            //Add quotes to memory and update them
          }

          print_r($values[1]);

        // ---

            $loop_start_time = time();
            $loop_end_time = $loop_start_time + 60;
            while($loop_end_time > $loop_start_time){

                $q = quotes::where('primary_key', 'like', 'XWW30%')
                    ->orderBy('primary_key', 'desc')
                    ->take(10)->get();
                    echo $q[0]->bid . "</br>" . $q[0]->time . "</br>";
                    echo sizeof($q);

                Redis::set('XWW30', $q);

                $loop_start_time = time();
            };
            //event(new \App\Events\EventName($q));
        })->everyMinute();
      */
// ---

   /*
//Get quotes - UNFINISHED
      $schedule->call(function () {

        //Fork
        $pid = pcntl_fork();

        if (!$pid) {

          //CHILD
          $loop_start_time = time();
          $loop_end_time = $loop_start_time + 62;

          while($loop_end_time > $loop_start_time){

            $query = DB::table('quotes')->where('symbol', 'XWW30')->orderby('time', 'desc')->first();
            $query = json_encode($query);
            Redis::set('last_tick_XWW30', $query);

            $loop_start_time = time();
            usleep(200000);

          };
          exit();
        }
      })->everyMinute();
 */
// ---

        // $schedule->command('inspire')
        //          ->hourly();
        //          ->everyMinute();
    }
}
