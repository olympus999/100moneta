<?php

namespace App\Classes;

use Redis;
use Log;
use DB;

class SymbolsClass
{

  // Get enabled symbols from DB and store in Redis
  public function updateEnabledSymbolsToRedis()
  {
    $query = DB::table('symbols_settings')->get();
    for ($i = 0; $i < count($query); $i++){
      if ($query[$i]->enabled != 1){
        unset($query[$i]);
        $query = array_values($query);
        $i--;
      }
    }
    $query = json_encode($query);

    Redis::set('symbolsSettings', $query);
  }
}
