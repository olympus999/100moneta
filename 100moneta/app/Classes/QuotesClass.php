<?php

namespace App\Classes;

use Redis;
use Log;
use DB;
use \App\Classes\RedisClass;
use \App\Events\QuotesSendEvent;

class QuotesClass
{

  public function updateSymbolsQuotes()
  {
    $timeS5 = time() * 1000 - 5 * 1000;
    $loopQuotesTime = Redis::get('loopQuotesTime');
    if($loopQuotesTime < $timeS5)
    {
      $id = time();
      $pid = pcntl_fork();

      if (!$pid) {
        $quotesSendEvent = new QuotesSendEvent();

        while(true){

          $loop_start_time = time();
          $loop_end_time = $loop_start_time + 62;

          while($loop_end_time > $loop_start_time){
/*
            $query = DB::table('quotes_latest_ticks')
              ->where('symbol', 'XWW30')
              ->first();
*/

            $query = DB::select('select * from v_quotes_latest_enabled');
            /*$price = round(($query->bid + $query->ask)/2, $decimals);
            $query->price = $price;*/
            $quotesSendEvent->quotes($query);
            event($quotesSendEvent);
            //Log::info("In the loop");
            $loop_start_time = time();

            $this->checkLoopExitCriteria($id);
            $milliseconds = round(microtime(true) * 1000);
            Redis::set('loopQuotesTime', $milliseconds);
            usleep(200000);

          };
        };
      };
    };
  }

  protected function checkLoopExitCriteria($id)
  {
    $loopStatus = Redis::get('loopQuotesEnabled');

    if($loopStatus == '0')
    {
      Log::alert("Quitting quotes loop. ID = " . $id);
      exit();
    }
  }
}
