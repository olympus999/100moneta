<?php

namespace App\Classes;

use Redis;
use Log;
use \App\Events\ClientsTimeEvent;
use \App\Classes\SymbolsClass;

class NewSecondClass
{

  // New second, declare all actions related to new second here
  public function newSecond($t)
  {
    $this->updateClientTime($t);
    $this->updateEnabledSymbolsToRedis($t);
  }
  
  // Protected functions to be called from above

  protected function updateEnabledSymbolsToRedis()
  {
    $timeEvent = new SymbolsClass();
    $timeEvent->updateEnabledSymbolsToRedis();
  }

  protected function updateClientTime($t)
  {
    $timeEvent = new ClientsTimeEvent();
    $timeEvent->timeToClient($t);
    event($timeEvent);
  }
}
