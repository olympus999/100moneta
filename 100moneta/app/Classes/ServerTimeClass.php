<?php

namespace App\Classes;

use Redis;
use Log;
use \App\Classes\NewSecondClass;
use \App\Classes\New10SecondClass;
use \App\Classes\NewMinuteClass;
use \App\TradeSettingsModel;

class ServerTimeClass
{

  protected $nextFullSecond, $next10Second, $nextFullMinute,$quotesSendEvent,
    $newSecondClass, $new10SecondClass, $newMinuteClass;

  // Set needed classes
  public function __construct()
  {
    $this->newSecondClass = new NewSecondClass();
    $this->new10SecondClass = new new10SecondClass();
    $this->newMinuteClass = new NewMinuteClass();
  }

  // Emit event every second and minute
  public function serverTimeLoop()
  {
    $timeS5 = time() * 1000 - 5 * 1000;
    Log::alert("serverTimeLoop = " . Redis::get('serverTimeLoop') . ". timeS = " . $timeS5);
    if(Redis::get('serverTimeLoop') < $timeS5)
    {
      $pid = pcntl_fork();
      if (!$pid) {

        //CHILD
        $id = time();
        $timeUnixMs = round(microtime(true) * 1000);
        $this->nextFullMinute = ((floor(($timeUnixMs/1000)/60))*60 + 60) * 1 * 1000;
        $this->nextFullSecond = (floor($timeUnixMs/1000) + 1) * 1000;
        $this->next10Second   = floor($timeUnixMs/10000) * 10000;

        //LOOP
        while(true){

          $timeUnixMs = round(microtime(true) * 1000);

          // Check when new minute occurs
          if($this->nextFullMinute < $timeUnixMs )
          {
            $this->newMinute($timeUnixMs);
          }

          // Check when new 10 second occur
          if($this->next10Second < $timeUnixMs )
          {
            $this->new10Second();
          }

          // Check when new second occurs
          if($this->nextFullSecond < $timeUnixMs )
          {
            $this->newSecond($timeUnixMs);
          }

          Redis::set('serverTimeLoop', $timeUnixMs);

          //EXIT CRITERIA - Exit loop if redis loopStatus = 1
          $this->checkLoopExitCriteria($id);

          usleep(50000);
        }
        exit();
      }
    }
  }

  protected function checkLoopExitCriteria($id)
  {
    $loopStatus = Redis::get('loopStatus');
    if($loopStatus == '1')
    {
      Log::alert("intval(Redis::get('loopStatus')) = " . Redis::get('loopStatus'));
      Log::alert("Quitting. ID = " . $id);
      exit();
    }
  }

  protected function newMinute($t)
  {
    $this->nextFullMinute += 60*1000;
    $this->newMinuteClass->newMinute($t);
  }

  protected function new10Second()
  {
    $this->next10Second += 10*1000;
    $this->new10SecondClass->new10Second();
    //$newSecondEvent->
  }

  protected function newSecond($t)
  {
    $this->nextFullSecond += 1*1000;
    $this->newSecondClass->newSecond($t);
    //$newSecondEvent->
  }

}
