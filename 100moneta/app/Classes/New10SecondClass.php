<?php

namespace App\Classes;

use Redis;
use Log;
use \App\Events\ClientsTimeEvent;
use \App\Classes\SymbolsClass;

class New10SecondClass
{

  // New 10 second, declare all actions related to new second here
  public function new10Second()
  {
    $this->runQuotes();
  }

  // Protected functions to be called from above
  protected function runQuotes()
  {
    $timeEvent = new QuotesClass();
    $timeEvent->updateSymbolsQuotes();
  }
}
