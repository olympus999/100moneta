<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {

  // Temp and test routes

		Route::get('time', function () {
			$time_start = microtime(true);

			$query = DB::table('symbols_settings')->get();
			for ($i = 0; $i < count($query); $i++){
				if ($query[$i]->enabled != 1){
					unset($query[$i]);
					$query = array_values($query);
					$i--;
				}
			}
			$query = json_encode($query);
			Redis::set('symbolsSettings', $query);
			$d = Redis::get('symbolsSettings');
			$d = json_decode($d, true);

			$time_end = microtime(true);
			echo ($time_end - $time_start);
		});

		Route::get('enabledsymbols', function () {
			echo Redis::get('symbolsSettings');
		});

		Route::get('testquotes', function () {
				$query = DB::table('quotes_latest_ticks')->get();
				print_r($query);
		});

		Route::get('loopbutton/{status}', function ($status) {
				// this checks for the event
				echo Redis::set('loopStatus', $status);
				echo Redis::get('loopStatus');
		});

		Route::get('loopquotesenabled/{status}', function ($status) {
				// this checks for the event
				echo Redis::set('loopQuotesEnabled', $status);
				echo Redis::get('loopQuotesEnabled');
		});

		Route::get('test4', function () {
				// this checks for the event
				return view('test.test4-redis');
		});

		Route::get('test5', function () {
				// this checks for the event
				return view('test.test5-db');
		});

		Route::get('test6', function () {
				// this checks for the event
				echo Redis::set('loopStatus', 0);
				echo Redis::get('serverTimeLoop')."</br>";
				echo Redis::get('loopStatus');
		});

		// redis quotes test routes

		Route::get('fire', function () {
		    // this fires the event
				$serverTime = microtime();
		    event(new \App\Events\ServerTimeEvent($serverTime));
		    return "event fired";
		});

		Route::get('testevent', 'InitialConnectionController@InitialSettings');

		Route::get('/test', function ()    {
        return view('test.test3post');
    });

		Route::post('/post', 'test\PostController@testevent');

    Route::get('/hash', function ()    {
        $password = Hash::make('Aa123456');
        echo $password;
    });

    Route::get('/details', function ()    {
        $user = Auth::user();
        echo "User: " . $user->balance;
    });

  // ---


  // Authenticating user

    Route::get('/', function ()    {
        return view('auth.login');
    });

    Route::get('/home', function ()    {
        return view('home');
    });

    Route::get('/login', function ()    {
        return view('auth.login');
    });

    Route::get('/register', function ()    {
        return view('auth.register');
    });

  // ---

	// Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
	  Route::post('auth/login', 'Auth\AuthController@postLogin');
	  Route::get('auth/logout', 'Auth\AuthController@getLogout');

	// Registration routes...
	  Route::get('auth/register', 'Auth\AuthController@getRegister');
	  Route::post('auth/register', 'Auth\AuthController@postRegister');

});


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');

		Route::controllers([
				'password' => 'Auth\PasswordController',
		]);
});
