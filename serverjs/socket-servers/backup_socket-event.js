var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('channel', function(err, count) {
});

redis.on('message', function(channel, message) {

    message = JSON.parse(message);
    message = message.data;
    channel = channel + ":" + message.subChannel;
    //console.log("Channel = " + channel);
    io.emit(channel, message);
});

io.on('connection', function(socket){
  console.log('connected socket with id  = ' + socket.id);

  socket.on('disconnect', function(){
    console.log('disconnected id  = ' + socket.id);
  });
});

http.listen(3000, function(client){
    console.log('Listening on Port 3000');
});
