var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('channel', function(err, count) {
});

redis.on('message', function(channel, message) {

    message = JSON.parse(message);
    //console.log('message.data = ' + JSON.stringify(message));
    // Emit server time
    if(message.data.fn == 'updateClientsTime'){
      prepareServerTime(channel, message);
    }
    else if(message.data.fn == 'quotes'){
      prepareQuotes(channel, message);
    }

});

io.on('connection', function(socket){
  console.log('connected socket with id  = ' + socket.id);
  socket.join('XWW30');
  socket.join('servertime');
  socket.on('disconnect', function(){
    console.log('disconnected id  = ' + socket.id);
  });
  socket.on('symbol', function (data) {
    socket.join(data);
  });

});

http.listen(3000, function(client){
    console.log('Listening on Port 3000');
});

function prepareServerTime(channel, message){
  room = 'servertime';
  message = message.data;
  channel = channel + ":" + message.subChannel;
  emitData(channel, message, room);
}

function prepareQuotes(channel, message){
  console.log('message.data = ' + JSON.stringify(message));
  message = message.data;
  channel = channel + ":" + message.subChannel;
  message = JSON.parse(message.quotes);
  var length = message.length;
  console.log("length " + length);
  //console.log("message " + JSON.stringify(message[0]));
  for(j = 0; j < length; j++){
      room = message[j].symbol;
      data = message[j];
      console.log("room = " + room);
      console.log("data = " + data);
      emitData(channel, data, room);
  }

  //console.log("message = " + JSON.stringify(message));
  //message = JSON.parse(message.data.quotes.data);

  //channel = channel + ":" + message.subChannel;
  //console.log("Channel = " + channel);
  //io.to('XWW30').emit(channel, message);
  //io.emit(channel, message);
}

function emitData(channel, message, room){
  //io.to('some room').emit('some event'):
  io.to(room).emit(channel, message);
}
